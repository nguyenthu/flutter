import 'package:flutter/material.dart';

void main() => runApp(HelloWorld());

class HelloWorld extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: "Welcome to Flutter App",
      home: Scaffold(
        appBar: AppBar(
          title: Text("Welcome to Flutter")
        ),
        body: Center(
          child: Text("Hello world"),
),
      ),
    );
  }

}